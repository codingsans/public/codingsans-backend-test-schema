# CodingSans Backend Test Schema

The main goal is to create a web application which authenticates via JWT (Json Web Token) and serves an API which validates different kind of schemas.

For the web serving part you could use either

- [Koa](https://koajs.com/)
- [Express](https://expressjs.com/)

Please include a middleware for logging on all requests.

- https://github.com/koajs/logger
- https://github.com/expressjs/morgan

The login endpoint ( `POST /login` ) should return a signed JWT token on correct username/password.

The token should be signed with https://github.com/auth0/node-jsonwebtoken and the signing secret should be provided via config.

The POST request's body will contain `{ username: string, password: string }`.

The correct username/password combination should be provided via config, because the application should be database-less.

For config handling we recommend our guide: https://codingsans.com/blog/node-config-best-practices

The schema validator endpoint ( `PATCH /schemavalidator` ) should be guarded by a JWT middleware.

- https://github.com/koajs/jwt
- https://github.com/auth0/express-jwt

For the schema validating we recommend to use the ajv schema validator https://github.com/epoberezkin/ajv ( wrapped in a middleware ) but we are open for any other solutions.

The data should be sent in the request's body.

The acceptable schemas for the ( `PATCH /schemavalidator` ) endpoints are the following TS interfaces:

`{ squad: { squadName: string; homeTown: string; formed: number; active: boolean; } }`

`{ teamMember: { name: string; age: number; secretIdentity: string; active: boolean; powers: string[]; } }`

If the schema is accepted the server should return `200 OK` and the response body should contain `{ "msg": "the schema is acceptable" }`
If the schema could not be accepted the server should return `400 Bad Request`

By acceptable we mean

- the request's body should be one of the given interfaces
- the request's body should contain all of the attributes defined in the interface - with the right type
- the request's body should not contain any additional properties

If the user called any other than `POST /login` or `PATCH /schemavalidator` the server should return `404 Not Found`.

Please use typescript, we included linting and testing in this project.

You can start the project with `yarn start`.
You can check the lint and formatting of the project with `yarn lint`.

If you are using VSCode you can use these extensions:

- https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig
- https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
